---
codename: 'Z00A'
name: 'Asus Zenfone 2 ZE551ML'
comment: 'wip'
icon: 'phone'
noinstall: true
maturity: 0
---

A community port has been started [here](https://forums.ubports.com/topic/3491/asus-z00a-zenfone-2-ze551ml-z00a).
