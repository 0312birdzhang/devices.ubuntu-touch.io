---
codename: 'librem5'
name: 'Librem 5'
comment: 'experimental'
icon: 'phone'
noinstall: true
maturity: .1
---

The Librem5 is a linux phone. An [experimental Ubuntu Touch image](https://ci.ubports.com/job/rootfs/job/rootfs-librem5/) is available.
