---
codename: 'oneplus3t'
name: 'Oneplus 3 t'
comment: 'community device'
icon: 'phone'
maturity: .8
---

You can install Ubuntu Touch on the Oneplus 3 t.